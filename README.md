# 进销存项目
- 完成了基本的系统设置，商品设置，客户，采购，入库，出库功能，用户管理功能
- 因为时间有限，不再维护，前端做得很烂
## 使用方法
本项目基于python27开发，需要安装request里面的组件
`pip install -r request.txt`
安装完成后使用
1. `python migrate.py db init`进行数据库的初始化
2. `python migrate.py db migrate`进行数据的建立
3. `python migrate.py db.upgrade` 写入到数据库
4. `python migrate.py init_user` 进行用户初始化
5. 帐号:admin 密码:123456
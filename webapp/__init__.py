#encoding:utf-8
from flask import Flask,Blueprint,redirect,url_for
from webapp.controller.admin import admin
from exis import db
from models import User
import config
from flask_login import LoginManager
from flask_bootstrap import Bootstrap


app = Flask(__name__)
app.config.from_object(config)
app.register_blueprint(admin,url_prefix='/admin')
db.init_app(app)
login_manager = LoginManager()
login_manager.session_protection="strong"
login_manager.login_view='admin.login'
login_manager.init_app(app)
bootstrap = Bootstrap(app)

@app.route('/')
def root():
    return redirect(url_for('admin.login'))

@login_manager.user_loader
def user_load(user_id):
    return User.query.get(int(user_id))
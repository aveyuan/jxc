#encoding:utf-8
from flask_wtf import FlaskForm
from wtforms import StringField,PasswordField,SubmitField,BooleanField,SelectField,IntegerField
from wtforms.validators import DataRequired,Length

#登录验证
class LoginForm(FlaskForm):
    username = StringField(u'用户名',validators=[DataRequired(u'用户名不能为空')])
    password = PasswordField(u'密码',validators=[DataRequired(u'密码不能为空')])
    remeberme = BooleanField(u'记住我')

#状态
class StatusForm(FlaskForm):
    name = username = StringField(u'状态:',validators=[DataRequired(u'状态不能为空')])

#信息来源
class InfosourceForm(FlaskForm):
    name = StringField(u'信息来源:',validators=[DataRequired(u'信息来源不能为空')])

#客户信息
class CommodityForm(FlaskForm):
    name =  StringField(u'商品名称:',validators=[DataRequired(u'姓名不能为空')])
    tcode = StringField(u'条码')
    scode = StringField(u'串号:')
    xingh = StringField(u'型号:')
    danw = StringField(u'单位:')
    shengcrq = StringField(u'生产日期:')
    baozq = StringField(u'保质期:')
    pih = StringField(u'批号:')
    shengcdz = StringField(u'生产地址:')
    shengcs = StringField(u'生产商:')
    note = StringField(u'备注:')

#商品属性
class ClientForm(FlaskForm):
    name =  StringField(u'姓名:',validators=[DataRequired(u'商品名称不能为空')])
    sex = SelectField(u'性别:',choices=[(1,u'男'),(0,u'女')],coerce=int)
    phone1 = StringField(u'电话1:')
    phone2 = StringField(u'电话2:')
    wechat = StringField(u'微信:')
    qq = StringField(u'QQ号:')
    brithday = StringField(u'生日:')
    address = StringField(u'地址:')
    infosources = SelectField(u'信息来源:',coerce=int)
    statuses = SelectField(u'客户状态:',coerce=int)
    note = StringField(u'备注:')

#状态信息
class StatusForm(FlaskForm):
    name =  StringField(u'状态:',validators=[DataRequired(u'不能为空')])

#信息来源
class InfosourceForm(FlaskForm):
    name =  StringField(u'状态:',validators=[DataRequired(u'不能为空')])
#商品采购
class CostorderForm(FlaskForm):
    caigsy = StringField(u'采购事由:', validators=[DataRequired(u'采购事由不能为空')])
    name = StringField(u'采购名称:')
    caigbh = StringField(u'采购编号:')
    caigrq = StringField(u'采购日期:')
    caigr = StringField(u'采购人:')
    note = StringField(u'备注:')
    commoditys = SelectField(u'采购商品:', coerce=int)
    caigjg = StringField(u'采购价格:')
    caigsl = StringField(u'采购数量:')

#商品单销售
class SaleorderForm(FlaskForm):
    salename= StringField(u'销售名称:')
    salebh = StringField(u'销售编号:')
    saledate = StringField(u'销售时间:')
    saler = StringField(u'销售人:')
    note = StringField(u'备注:')
    clients = SelectField(u'购买客户:', coerce=int)
    commoditys = SelectField(u'销售商品:', coerce=int)
    salejg = StringField(u'销售价格:')
    salesl = StringField(u'销售数量:')

#商品出库
class SaleorderckForm(FlaskForm):
    salebh= StringField(u'销售单号:')
    kdgs = StringField(u'快递公司:')
    kddh = StringField(u'快递单号:')
    cksj = StringField(u'出库时间:')
    note = StringField(u'备注:')
    fhr = SelectField(u'出库人:', coerce=int)

class Updateuser(FlaskForm):
    password1 = PasswordField(u'密码',validators=[DataRequired(u'密码不能为空')])
    password2 = PasswordField(u'重复密码', validators=[DataRequired(u'密码不能为空')])

class Adduser(FlaskForm):
    username = StringField(u'用户名',validators=[DataRequired(u'用户名不能为空')])
    nickname = StringField(u'昵称',validators=[DataRequired(u'昵称不能为空')])
    email = StringField(u'Email')
    password1 = PasswordField(u'密码',validators=[DataRequired(u'密码不能为空')])
    password2 = PasswordField(u'重复密码', validators=[DataRequired(u'密码不能为空')])
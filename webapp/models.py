#encoding:utf-8
from exis import db
from flask_login import UserMixin
from werkzeug.security import generate_password_hash,check_password_hash

#用户信息表
class User(db.Model,UserMixin):
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    username = db.Column(db.String(50))
    password_hash = db.Column(db.String(255))
    nickname = db.Column(db.String(50))
    email = db.Column(db.String(50))

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')
    @password.setter
    def password(self,password):
        self.password_hash = generate_password_hash(password)
    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

#客户表
class Client(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(20))
    sex = db.Column(db.SmallInteger)
    phone1 = db.Column(db.BigInteger)
    phone2 = db.Column(db.BigInteger)
    wechat = db.Column(db.String(50))
    qq = db.Column(db.BigInteger)
    brithdy = db.Column(db.BigInteger)
    address = db.Column(db.String(150))
    note = db.Column(db.String(255))
    infosource_id=db.Column(db.Integer,db.ForeignKey('infosource.id'))
    infosource = db.relationship('Infosource', backref=db.backref('clients'))
    status_id = db.Column(db.Integer,db.ForeignKey('status.id'))
    statuses = db.relationship('Status',backref=db.backref('clients'))

#信息来源表
class Infosource(db.Model):
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    name = db.Column(db.String(50))

#状态表
class Status(db.Model):
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    name = db.Column(db.String(50))

#商品表
class Commodity(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(50))
    tcode = db.Column(db.String(50))
    scode = db.Column(db.String(50))
    xingh = db.Column(db.String(50))
    danw = db.Column(db.String(50))
    shengcrq = db.Column(db.String(50))
    baozq = db.Column(db.String(30))
    pih = db.Column(db.String(30))
    shengcdz =db.Column(db.String(50))
    shengcs = db.Column(db.String(30))
    note = db.Column(db.String(255))

#采购单
class Costorder(db.Model):
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    caigsy  = db.Column(db.String(255))
    name = db.Column(db.String(50))
    caigbh = db.Column(db.String(50))
    date = db.Column(db.String(50))
    caigr = db.Column(db.String(20))
    note = db.Column(db.String(255))

#采购列表
class Costlist(db.Model):
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    commodity_id = db.Column(db.Integer,db.ForeignKey('commodity.id'))
    caigjg = db.Column(db.Float(8))
    caigsl = db.Column(db.Integer)
    costorder_id = db.Column(db.Integer,db.ForeignKey('costorder.id'))
    commoditys = db.relationship('Commodity',backref=db.backref('costlists'))
    costorders = db.relationship('Costorder',backref=db.backref('costlists'))

#销售单
class Saleorder(db.Model):
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    salename = db.Column(db.String(50))
    salebh = db.Column(db.String(50))
    saledate = db.Column(db.String(50))
    saler = db.Column(db.String(20))
    note = db.Column(db.String(255))
    client_id = db.Column(db.Integer,db.ForeignKey('client.id'))
    clients = db.relationship('Client',backref=db.backref('saleorders'))

#销售单列表（实现快照功能，因为商品的生产日期，批号，保质期等都会变的）
class Salelist(db.Model):
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    commodity_id = db.Column(db.Integer,db.ForeignKey('commodity.id'))
    c_name = db.Column(db.String(50))
    c_tcode = db.Column(db.String(50))
    c_scode = db.Column(db.String(50))
    c_xingh = db.Column(db.String(50))
    c_danw = db.Column(db.String(50))
    c_shengcrq = db.Column(db.String(50))
    c_baozq = db.Column(db.String(30))
    c_pih = db.Column(db.String(30))
    c_shengcdz =db.Column(db.String(50))
    c_shengcs = db.Column(db.String(30))
    salejg = db.Column(db.Float(8))
    salesl = db.Column(db.Integer)
    saleorder_id = db.Column(db.Integer,db.ForeignKey('saleorder.id'))
    commoditys = db.relationship('Commodity',backref=db.backref('salelists'))
    saleorders = db.relationship('Saleorder',backref=db.backref('salelists'))

#出库
class Commodityck(db.Model):
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    kddh = db.Column(db.String(50))
    kdgs = db.Column(db.String(50))
    user_id = db.Column(db.Integer,db.ForeignKey('user.id'))
    users = db.relationship('User', backref=db.backref('commoditycks'))
    fhdate = db.Column(db.String(50))
    saleorder_id = db.Column(db.Integer,db.ForeignKey('saleorder.id'))
    saleorders = db.relationship('Saleorder', backref=db.backref('commoditycks'))
    note = db.Column(db.String(255))